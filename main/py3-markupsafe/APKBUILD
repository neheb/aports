# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-markupsafe
_pkgname=MarkupSafe
pkgver=2.1.3
pkgrel=1
pkgdesc="Implements a XML/HTML/XHTML Markup safe string"
url="https://github.com/pallets/markupsafe"
arch="all"
license="BSD-3-Clause"
depends="python3"
makedepends="python3-dev py3-gpep517 py3-setuptools py3-wheel"
checkdepends="pytest"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-markupsafe" # Backwards compatibility
provides="py-markupsafe=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel --wheel-dir .dist --output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
	find "$pkgdir" -name "*.c" -delete
}

sha512sums="
97dcfa9277c8b34e5ebf899069f690452e90943e0f84ba8ffac725263d84e7c2b782294f9f386be31e8b6846505f0ec70835e3965fc76a1ce07b19432de4a7de  MarkupSafe-2.1.3.tar.gz
"
